# Logistic

## Instalation

- Install Node js
- Install XAMPP or similiar

## How to run
Start MySQL in XAMPP

Open CMD in Root Project

Install Node Modules:

```sh
npm install
```

Run project:

```sh
npm run start
```

then you can open http://localhost:8080/swagger
```sh
127.0.0.1:8080/swagger
```
