const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const moment = require('moment');
const routes = require('./src/routes');
const swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger.json');
const swaggerJSDoc = require('swagger-jsdoc');

// Set Time to local
moment.locale();

// Body Parser = for parsing application/json & for parsing application/xwww-
app.use(bodyParser.json())
//app.use(bodyParser.urlencoded())
app.use(bodyParser.urlencoded({extended: true}))

// FileUpload = for parsing multipart/form-data
app.use(fileUpload())

// CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    if (req.method === 'OPTIONS') {
        res.header(
            'Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, PUT'
        );
        return res.status(200).json({});
    }
    next();
});

app.use('/swagger', 
    swaggerUi.serve, 
    swaggerUi.setup(swaggerDocument),
);

app.use(routes);

module.exports = app
