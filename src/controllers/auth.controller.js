const { verify } = require("jsonwebtoken");
const { loginService, registerService } = require("../services/auth.service");
const { badRequest, basicResponse } = require("../utils/response");
const { verifyToken } = require("../utils/jwt");

const login = async (req, res, next) => {

    const { msisdn, password } = req.body;

    const { data, error } = await loginService(msisdn, password);
    if (error) return badRequest({ res }, error);

    return basicResponse({ res }, {
        token: data
    });
}

const register = async (req, res, next) => {

    const { msisdn, username, name, password } = req.body;

    const { data, error } = await registerService(msisdn, username, password, name);
    if (error) return badRequest({ res }, error);

    return basicResponse({ res }, data);

}

const claims = async (req, res, next) => {

    return basicResponse({ res }, {
        id: req.user_data.id
    })
}

module.exports = {
    login,
    register,
    claims
}