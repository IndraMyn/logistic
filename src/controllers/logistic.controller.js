const { getLogistic, createLogistic } = require("../services/logistic.service");
const { basicResponse, badRequest } = require("../utils/response");

const create = async (req, res, next) => {

    const { logistic_name, destination_name, origin_name, amount, duration } = req.body;

    const { data, error } = await createLogistic(logistic_name, origin_name, destination_name, amount, duration);
    if (error) return badRequest({ res }, error);

    return basicResponse({ res }, data);

}

const get = async (req, res, next) => {

    const { destination_name, origin_name } = req.query;

    const { data, error } = await getLogistic(origin_name, destination_name);
    if (error) return badRequest({ res }, error);
    
    return basicResponse({ res }, data);
}

module.exports = {
    create, 
    get
}