const express = require("express");
const { badRequest } = require("../utils/response");
const { verifyToken } = require("../utils/jwt");
const error = require("../utils/error");
const authorizeMiddleware = express.Router();

authorizeMiddleware.use(async (req, res, next) => {

    if (req.headers.authorization) {

        const authorization = req.headers.authorization;
        const token = authorization.split(" ")[1];

        const verify = await verifyToken(token);
        if (!verify) return badRequest({ res }, error.notAuthorization, undefined, 401);

        req.user_data = verify;
        return next();

    } else {

        return badRequest({ res }, error.accessForbidden, undefined, 403);

    }

});

module.exports = authorizeMiddleware;