'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('logistic', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
      },
      logistic_name: {
          type: Sequelize.STRING,
          allowNull: false
      },
      amount: {
          type: Sequelize.INTEGER,
          allowNull: false
      },
      destination_name: {
          type: Sequelize.STRING,
          allowNull: false
      },
      origin_name: {
          type: Sequelize.STRING,
          allowNull: false
      },
      duration: {
          type: Sequelize.STRING,
          allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
