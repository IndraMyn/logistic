const express = require('express');
const { login, register, claims } = require('../controllers/auth.controller');
const authorizeMiddleware = require('../middlewares/authorize.middleware');
const authValidator = require('../validators/auth.validator');
const authRoutes = express.Router();

authRoutes.post('/login', authValidator.login, authValidator.verify, login);
authRoutes.post('/register', authValidator.register, authValidator.verify, register);
authRoutes.post('/claims', authorizeMiddleware, claims);

module.exports = authRoutes;