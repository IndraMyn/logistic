const express = require('express');
const authRoutes = require('./auth.routes');
const logisticRoutes = require('./logistic.routes');
const authorizeMiddleware = require('../middlewares/authorize.middleware');
const routes = express.Router();

routes.use("/auth", authRoutes);
routes.use("/logistic", authorizeMiddleware, logisticRoutes);

module.exports = routes;