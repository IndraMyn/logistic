const express = require('express');
const { get, create } = require('../controllers/logistic.controller');
const logisticValidator = require('../validators/logistic.validator');
const logisticRoutes = express.Router();

logisticRoutes.get('/', logisticValidator.getLogistic, logisticValidator.verify, get);
logisticRoutes.post('/', logisticValidator.createLogistic, logisticValidator.verify, create);

module.exports = logisticRoutes;