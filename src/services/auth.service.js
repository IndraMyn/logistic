const userModel = require("../models/user.model");
const { generateToken } = require("../utils/jwt");
const error = require('../utils/error');
const { Op } = require("sequelize");
const crypto = require('crypto');

/**
 * Login Service
 * @param {number} msisdn 
 * @param {string} username 
 * @param {string} password 
 * @param {string} name 
 */
const loginService = async (msisdn, password) => {

    const encryptPassword = crypto.createHash("sha1").update(password).digest("hex");

    const user = await userModel.findOne({
        where: {
            msisdn: msisdn,
        }
    });

    if (user == null || user.password != encryptPassword) {
        return {
            data: null,
            error: error.invalidCredentials
        }
    }
    
    const token = generateToken({
        id: user.id
    });

    return {
        data: token,
        error: null
    }
    
}

/**
 * Register Service
 * @param {number} msisdn 
 * @param {string} username 
 * @param {string} password 
 * @param {string} name 
 */
const registerService = async (msisdn, username, password, name) => {

    const user = await userModel.findOne({
        where: {
            [Op.or] : {
                username: username,
                msisdn: msisdn,
            }
        }
    });


    if (user != null) return {
        data: null,
        error: error.userAlreadyExist
    };

    const encryptPassword = crypto.createHash("sha1").update(password).digest("hex");

    const create = await userModel.create({
        username: username,
        msisdn: msisdn,
        name: name,
        password: encryptPassword,
    });

    return {
        data: {
            username: username,
            msisdn: msisdn,
            name: name,
        },
        error: null
    };

}

module.exports = {
    loginService,
    registerService
}