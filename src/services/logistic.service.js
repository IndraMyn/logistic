const logisticModel = require("../models/logistic.model")

/**
 * Get data logistic by origin name and destination name
 * @param {string} originName 
 * @param {string} destinationName 
 */
const getLogistic = async (originName, destinationName) => {

    const logistic = await logisticModel.findAll({
        where: {
            origin_name: originName,
            destination_name: destinationName,
        }
    });


    return {
        data: logistic,
        error: null
    };

}

/**
 * Create new data logistic
 * @param {string} logisticName 
 * @param {string} originName 
 * @param {string} destinationName 
 * @param {integer} amount 
 * @param {string} duration 
 */
const createLogistic = async (logisticName, originName, destinationName, amount, duration) => {

    const create = await logisticModel.create({
        logistic_name: logisticName,
        origin_name: originName,
        destination_name: destinationName,
        amount: amount,
        duration: duration
    });

    return {
        data: create.dataValues,
        error: null
    };
}

module.exports = {
    getLogistic,
    createLogistic
}