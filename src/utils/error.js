class Error {

    static internalServer = {
        code: 999,
        message: "Internal Server Error"
    }

    static dbExecption = {
        code: 1000,
        message: "Database Exeception"
    }

    static userAlreadyExist = {
        code: 1001,
        message: "User already exist"
    }

    static invalidCredentials = {
        code: 1002,
        message: "msisdn or password is wrong!"
    }

    static notAuthorization = {
        code: 1003,
        message: "Not Authorization"
    }

    static accessForbidden = {
        code: 1004,
        message: "Access Forbidden"
    }



}

module.exports = Error;