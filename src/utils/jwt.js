const JWT = require('jsonwebtoken');

const key = process.env.JWT_KEY;

exports.verifyToken = (token) => {

    return new Promise((resolve, reject) => {
        JWT.verify(token, key, (err, decode) => {
            if (!err) {
                resolve(decode);
            } else {
                reject(null);
            }
        })
    }) 
}

exports.generateToken = (data) => {
    return JWT.sign(data, key)
}
