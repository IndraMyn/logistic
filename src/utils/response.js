/**
 * 
 * @param {*} config
 * @param {*} data 
 * @param {string} message 
 * @param {number} status 
 */

exports.basicResponse = ({ res }, data = true, message = 'Successfully', status = 200) => {
    
    res.status(status).json({
        data: data,
        message: message
    });

}

/**
 * 
 * @param {*} config
 * @param {object} error 
 * @param {string} message 
 * @param {number} status 
 */

exports.badRequest = ({ res }, error = null, message = 'Bad Request', status = 400 ) => {
    
    res.status(status).json({
        message: message,
        error: error
    });

}

/**
 * 
 * @param {*} config 
 * @param {Array} error 
 * @param {string} message 
 */

exports.validationError = ({ res }, error = [], message = 'Validation Error') => {

    res.status(433).json({
        message: message,
        error: error
    });

}
