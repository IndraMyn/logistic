const { query, param, body, validationResult } = require('express-validator');
const { validationError } = require('../utils/response');

const authValidator = {

    login: [
        body('msisdn').notEmpty(),
        body('password').notEmpty()
    ],
    register: [
        body('msisdn').notEmpty()
        .custom(val => {
            if (val.toString().indexOf("62") == 0) return true;

            return false;
        }).withMessage("msisdn must begin with the number 62"),
        body('username').notEmpty(),
        body('password').notEmpty(),
        body('name').notEmpty(),
    ],

    verify: (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return validationError({ res }, errors.array());
        } else {
            return next();
        }

    }

};

module.exports = authValidator;
