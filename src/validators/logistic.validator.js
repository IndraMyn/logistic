const { query, param, body, validationResult } = require('express-validator');
const { validationError } = require('../utils/response');

const logisticValidator = {

    getLogistic: [
        query('origin_name').notEmpty(),
        query('destination_name').notEmpty()
    ],
    createLogistic: [
        body('origin_name').notEmpty(),
        body('destination_name').notEmpty(),
        body('logistic_name').notEmpty(),
        body('amount').notEmpty(),
        body('duration').notEmpty(),
    ],

    verify: (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return validationError({ res }, errors.array());
        } else {
            return next();
        }

    }

};

module.exports = logisticValidator;
